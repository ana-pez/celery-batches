.. :changelog:

Changelog
#########

0.2 2018-04-20
==============

* Add support for protocol v2.
* Adds tests.
* Fixes some documentation issues.

0.1 2018-03-23
==============

* The initial released version, includes changes to make it a separate package,
  etc.
* Batch tasks now call pre- and post-run signals.

celery-final
============

* The final version of ``celery.contrib.batches`` before it was removed in
  4b3ab708778a3772d24bb39142b7e9d5b94c488b.
